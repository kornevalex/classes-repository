import { Person } from './classes/Person'
import Student from './classes/Student'
import Collaborator from './classes/Collaborator'
import Manager from './classes/Manager'

const myPerson = new Person('kornev', 'alex')
const myStudent = new Student('kornev', 'alex', new Date(), 4)
const myCollaborator = new Collaborator('kornev', 'alex', '4 копейки(Может машины?)', ['Какой-то список'])
const myManager = new Manager('kornev', 'alex', '4 копейки(Может машины?)', ['10 января получил 20 000', '10 февраля получил 110 000 рублей'], 5)

console.log(myStudent.info, myCollaborator.info, myManager.info, Person.count)
myPerson.info