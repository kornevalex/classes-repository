import Collaborator from './Collaborator'

class Manager extends Collaborator {
    constructor(surname, name, wage, listWage, listTeammate) {
        super(surname, name, wage, listWage)
        this.listTeammate = listTeammate
    }
}

export default Manager