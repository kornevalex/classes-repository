import {Person} from './Person'

 class Collaborator extends Person {
    constructor(surname, name, wage, listWage) {
        super(surname, name)
        this.wage = wage
        this.listWage = listWage
    }
}
export default Collaborator