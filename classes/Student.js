import {Person} from './Person'

class Student extends Person {
    constructor(surname, name, bornDate, course) {
        super(surname, name)
        this.bornDate = bornDate
        this.course = course
    }
}
export default Student