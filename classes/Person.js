export class Person {
    static count = 0;
    constructor(surname, name) {
        this.surname = surname
        this.name = name
        Person.count++
    }
    get info() {
        return console.log(this)
    }
    getCountPerson() {
        return Person.count
    }

}